# CarCar

Team:

* Person 1 - Which microservice?
* Eli - Sales

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.

## Sales microservice

For my models I created an AutomobileVO, Salesperson, Customer, and Sale model as required by the MVP. I added the AutomobileVO, Salesperson, and Customer models as foreign-keys to the Sale model as the sale model required all of the data from the three to produce a sale.

I created an encoders file for my AutomobileVO, Salesperson, Customer, and Sale information which will put it into the proper JSON form for me to use.

Next, in my views I created the necessary views to list and delete my salespeople, customers, and sales. In my URLs, I linked everything to its proper view and set the URL to the correct name as determined in learn.

With my poller, I am grabbing the automobiles from the inventory and putting the information into my AutomobileVO.

In my react portion of the project, I used the models, views, and the information from the inventory microservice to set up my forms and lists for all of the sales, customers, salespeople, and salespeople history.
