import React, {useState, useEffect} from 'react'

async function fetchData() {
    const customersFetch = await fetch('http://localhost:8090/api/customers/');
    if (customersFetch.ok) {
        const customersData = await customersFetch.json();
        return customersData;
    } else {
        console.error('Error fetching customer data');
    }
}

function CustomerList() {
    const[customerData, setCustomerData] = useState([]);

    useEffect(()=>{
        fetchData().then(data => {
            setCustomerData(data.customers);
        })
    }, []);

    async function deleteCustomer(id) {
        const customerURL = `http://localhost:8090/api/customers/${id}`;
        const fetchConfig = {
            method: 'DELETE',
        };
        const response = await fetch(customerURL, fetchConfig);
        if (response.ok) {
            window.location.reload(false);
        }
    }

    return (
        <div>
            <h1 className="text-center">Customers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Phone Number</th>
                    <th>Address</th>
                    </tr>
                </thead>
                <tbody>
                    {customerData.map(customer => {
                    return (
                        <tr key={customer.id}>
                            <td>{ customer.first_name }</td>
                            <td>{ customer.last_name }</td>
                            <td>{ customer.phone_number }</td>
                            <td>{ customer.address }</td>
                            <td><button className="btn btn-danger" onClick={() => {deleteCustomer(customer.id)}}>Delete</button></td>
                        </tr>
                    );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default CustomerList;
