import React, {useState, useEffect} from 'react'

async function fetchData() {
    const automobileFetch = await fetch('http://localhost:8100/api/automobiles/');
    if (automobileFetch.ok) {
        const automobileData = await automobileFetch.json();
        return automobileData;
    } else {
        console.error('Error fetching sale data');
    }
}

function AutomobileList() {
    const[automobileData, setAutomobileData] = useState([]);

    useEffect(()=>{
        fetchData().then(data => {
            setAutomobileData(data.autos);
        })
    }, []);

    async function deleteAutomobile(id) {
        const automobileURL = `http://localhost:8090/api/automobiles/${id}`;
        const fetchConfig = {
            method: 'DELETE',
        };
        const response = await fetch(automobileURL, fetchConfig);
        if (response.ok) {
            window.location.reload(false);
        }
    }

    return (
        <div>
            <h1 className="text-center">Automobiles</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                    <th>VIN</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                    <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {automobileData.map(auto => {
                    return (
                        <tr key={auto.id}>
                            <td>{ auto.vin }</td>
                            <td>{ auto.color }</td>
                            <td>{ auto.year }</td>
                            <td>{ auto.model.name }</td>
                            <td>{ auto.model.manufacturer.name }</td>
                            <td>{ auto.sold ? "Yes" : "No" }</td>
                            <td><button className="btn btn-danger" onClick={() => {deleteAutomobile(auto.id)}}>Delete</button></td>
                        </tr>
                    );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default AutomobileList;
