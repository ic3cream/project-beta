import React, {useState} from 'react';

function TechnicianForm(){
    const [firstName, setFirstName] = useState('');
    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }
    const [lastName, setLastName] = useState('');
    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }
    const [employeeID,setEmployeeID] = useState('');
    const handleEmployeeIDChange = (event) => {
        const value = event.target.value;
        setEmployeeID(value);
    }
    const handleSubmit = async(event) => {
        event.preventDefault();

        const data = {};
        data.firstName = firstName;
        data.lastName = lastName;
        data.employeeID = employeeID;

        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok){
            const newCustomer = await response.json();

            setFirstName('');
            setLastName('');
            setEmployeeID('');

        }
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className='shawdow p-4 mt-4'>
                    <h1>Add a Technician</h1>
                    <form onSubmit={handleSubmit} id='create-technician-form'>
                        <div className='form-floating mb-3'>
                            <input value={firstName} onChange={handleFirstNameChange} placeholder='First name' required type='text' name='first_name' id='first_name' className='form-control'/>
                            <label htmlFor='first_name'>First name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input value={lastName} onChange={handleLastNameChange} placeholder='Last name' required type ='text' name='last_name' className='form-control'/>
                            <label htmlFor='last_name'>Last name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input value={employeeID} onChange={handleEmployeeIDChange} placeholder='Employee id' required type ='text' name='employee_id' className='form-control'/>
                            <label htmlFor='employee_id'>Employee id</label>
                        </div>
                        <button className='btn btn-primary'>Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default TechnicianForm;
