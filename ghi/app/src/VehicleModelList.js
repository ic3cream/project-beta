import React, {useState, useEffect} from 'react'

async function fetchData() {
    const modelFetch = await fetch('http://localhost:8100/api/models/');
    if (modelFetch.ok) {
        const modelData = await modelFetch.json();
        return modelData;
    } else {
        console.error('Error fetching model data');
    }
}

function VehicleList() {
    const[modelData, setModelData] = useState([]);

    useEffect(()=>{
        fetchData().then(data => {
            setModelData(data.models);
        })
    }, []);

    async function deleteVehicle(id) {
        const vehicleURL = `http://localhost:8100/api/models/${id}`;
        const fetchConfig = {
            method: 'DELETE',
        };
        const response = await fetch(vehicleURL, fetchConfig);
        if (response.ok) {
            window.location.reload(false);
        }
    }

    return (
        <div>
            <h1 className="text-center">Models</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {modelData.map(model => {
                    return (
                        <tr key={model.id}>
                            <td>{model.name }</td>
                            <td>{ model.manufacturer.name }</td>
                            <td><img src={ model.picture_url } alt="Model Picture" style={{ maxHeight: '150px', maxWidth: '150px', width: 'auto', height: 'auto' }}/></td>
                            <td><button className="btn btn-danger" onClick={() => {deleteVehicle(model.id)}}>Delete</button></td>
                        </tr>
                    );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default VehicleList;
