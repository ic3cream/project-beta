import React, {useState, useEffect} from 'react'

async function fetchData() {
    const manufacturerFetch = await fetch('http://localhost:8100/api/manufacturers/');
    if (manufacturerFetch.ok) {
        const manufacturerData = await manufacturerFetch.json();
        return manufacturerData;
    } else {
        console.error('Error fetching manufacturer data');
    }
}

function ManufacturerList() {
    const[manufacturerData, setManufacturerData] = useState([]);

    useEffect(()=>{
        fetchData().then(data => {
            setManufacturerData(data.manufacturers);
        })
    }, []);

    async function deleteManufacturer(id) {
        const manufacturerURL = `http://localhost:8100/api/manufacturers/${id}`;
        const fetchConfig = {
            method: 'DELETE',
        };
        const response = await fetch(manufacturerURL, fetchConfig);
        if (response.ok) {
            window.location.reload(false);
        }
    }

    return (
        <div>
            <h1 className="text-center">Manufacturers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                    <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturerData.map(manufacturer => {
                    return (
                        <tr key={manufacturer.id}>
                            <td>{ manufacturer.name }</td>
                            <td><button className="btn btn-danger" onClick={() => {deleteManufacturer(manufacturer.id)}}>Delete</button></td>
                        </tr>
                    );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default ManufacturerList;
