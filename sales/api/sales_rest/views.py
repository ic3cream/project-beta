from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import AutomobileVO, Sale, Salesperson, Customer
from .encoders import SaleListEncoder, CustomerListEncoder, SalespersonListEncoder
from django.http import Http404
# Create your views here.

@require_http_methods(["GET", "POST"])
def list_salespeople(request):
    if request.method == "GET":
        try:
            salespeople = Salesperson.objects.all()
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Model does not exist"},
                status=400,
            )
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonListEncoder,
        )
    else:
        content = json.loads(request.body)

        salespeople = Salesperson.objects.create(**content)
        return JsonResponse(
            salespeople,
            encoder=SalespersonListEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def show_salespeople(request, pk):
    if request.method == "DELETE":
        try:
            salespeople = Salesperson.objects.get(id=pk)
            salespeople.delete()
            return JsonResponse({"deleted": True}, status=200)
        except Salesperson.DoesNotExist:
            raise Http404("Salespeople not found")


@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        try:
            customers = Customer.objects.all()
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Model does not exist"},
                status=400,
            )
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder,
        )
    else:
        content = json.loads(request.body)

        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerListEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def show_customer(request, pk):
    if request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse({"deleted": True}, status=200)
        except Customer.DoesNotExist:
            raise Http404("Customer not found")

@require_http_methods(["GET", "POST"])
def list_sales(request, automobile_vo_id=None):
    if request.method == "GET":
        try:
            if automobile_vo_id is not None:
                sales = Sale.objects.filter(automobile=automobile_vo_id)
            else:
                sales = Sale.objects.all()
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Model does not exist"},
                status=400,
            )
        return JsonResponse(
            {"sales": sales},
            encoder=SaleListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            automobile_href = content["automobile"]
            automobile = AutomobileVO.objects.get(import_href=automobile_href)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile id"},
                status=400,
            )
        try:
            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson id"},
                status=400,
            )
        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id"},
                status=400,
            )

        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleListEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def show_sale(request, pk):
    if request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=pk)
            sale.delete()
            return JsonResponse({"deleted": True}, status=200)
        except Sale.DoesNotExist:
            raise Http404("Sale not found")
