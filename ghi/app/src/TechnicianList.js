import React, {useState, useEffect} from "react";

async function fetchData() {
    const technicianfetch = await fetch('http://localhost:8080/api/technicians/');
    if (customersFetch.ok){
        const technicianData = await  technicianfetch.json();
        return technicianData;
    } else {
        console.error('Error fetching technician data');
    }
}

function TechnicianList() {
    const[technicianrData, setTechnicianData] = useState([]);

    useEffect (() => {
        fetchData().then(data =>{
            setTechnicianData(data.technician);
        }
            )
    }, []);

    async function deleteTechnician(id) {
        const technicianUrl = 'http://localhost:8080/api/technicians/${id}';
        const fetchConfig = {
            method: 'DELETE',
        };
        const response = await fetch(technicianUrl,fetchConfig);
        if (response.ok) {
            window.location.reload(false);
        }
    }

    return (
        <div>
            <h1 className="text-center">Technicians</h1>
            <table className="table tabled-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee id</th>
                    </tr>
                </thead>
                <tbody>
                    {technicianrData.map(technician => {
                        return (
                            <tr key={technician.id}>
                                <td>{technician.first_name}</td>
                                <td>{technician.last_name}</td>
                                <td>{technician.employee_id}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>

    )

}

export default TechnicianList;
